<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;

class Products extends Controller
{
    public function create()
    {
        return view('index');
    }
    public function products()
    {
        $products = DB::table('products')->limit(10)->get();
        return view('products')->with('products', $products);
    }
    public function details($id)
    {
        $product = Product::whereid($id)->first();
        return view('details')->with('product', $product);
    }
    public function edit($id)
    {
        $product = Product::whereid($id)->first();
        return view('edit')->with('product', $product);
    }
    public function save(Request $request)
    {
        $data = $request->validate([
          'product_name' => ['required','string','max:100'],
          'product_price' => ['required','integer','max:2000000000'],
          'manufacturing_date' => ['required', 'date']
        ]);
        Product::create([
          'product_name' => $data['product_name'],
          'product_price' => $data['product_price'],
          'manufacturing_date' => $data['manufacturing_date']
        ]);
        return back()->with('success', 'New Product created Successfully!');
    }
    public function update(Request $request)
    {
        $data = $request->validate([
          'product_name' => ['required','string','max:100'],
          'product_price' => ['required','integer','max:2000000000'],
          'manufacturing_date' => ['required', 'date'],
          'id' => ['required','integer']
        ]);
        $product = Product::find($data['id']);
        $product->product_name = $data['product_name'];
        $product->product_price = $data['product_price'];
        $product->manufacturing_date = $data['manufacturing_date'];
        $product->save();
        return back()->with('success', 'Product '.$data['product_name'].' updated Successfully!');
    }
}
