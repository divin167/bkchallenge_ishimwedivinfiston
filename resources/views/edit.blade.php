<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" integrity="sha256-bLNUHzSMEvxBhoysBE7EXYlIrmo7+n7F4oJra1IgOaM=" crossorigin="anonymous" />
    <title>BK Code Challenge</title>
  </head>
  <!-- <body style="background:url('{{asset('bg.jpg')}}'); background-repeat: no-repeat;background-attachment: fixed;"> -->
  <body>
    <div class="container">
      <h2 class="text-center mb-3">Product Update Form</h2>
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 card pt-3">
          @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
	             <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
             </div>
          @endif
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form method="POST" action="{{url('/update')}}">
            @csrf
            <div class="form-group">
              <label>Name</label>
              <input type="hidden" value="{{$product->id}}" name="id"/>
              <input type="text" name="product_name" value="{{$product->product_name}}" class="form-control" placeholder="product name">
            </div>
            <div class="form-group">
              <label>Price</label>
              <input type="text" name="product_price" value="{{$product->product_price}}" class="form-control" placeholder="product price">
            </div>
            <div class="form-group">
              <label>Manufacturing Date</label>
              <input id="manf_date" type="text" name="manufacturing_date" value="{{$product->manufacturing_date}}" class="form-control" placeholder="Manufacturing date">
            </div>
            <div class="form-group">
              <input type="submit" name="create_product" value="Update" class="form-control btn btn-success" placeholder="Manufacturing date">
            </div>
          </form>
          <a href="/">Back</a>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js" integrity="sha256-JIBDRWRB0n67sjMusTy4xZ9L09V8BINF0nd/UUUOi48=" crossorigin="anonymous"></script>
    <script>
    $('#manf_date').datepicker({
      format:'yyyy-mm-dd'
    });
    </script>
  </body>
</html>
