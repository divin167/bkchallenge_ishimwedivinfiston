<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Products@products');
Route::get('/create', 'Products@create');
Route::get('/edit/{id?}', 'Products@edit');
Route::post('/create', 'Products@save');
Route::post('/update', 'Products@update');
Route::get('/product/{id?}', 'Products@details');
